const {MessageEmbed} = require('discord.js')
const {parse} = require('url');
const xp = new Set();

module.exports = async (client, message) => {
    if (message.author.bot) return;
    if (message.channel.type === 'dm') return client.emit('directMessage', (client, message));

    let guildConfig = await client.models.guild.findOne({ id: message.guild.id });

    if (!guildConfig) {
        await client.functions.initGuild(client, message.guild, null);
        guildConfig = await client.models.guild.findOne({ id: message.guild.id });
    }

    let prefix = guildConfig.prefix;
    if (!prefix) prefix = client.settings.prefix;

    let args = message.content.slice(prefix.length).split(/ +/g);

    if (message.embeds.length > 0 && guildConfig.antiRaid.antiSelfBot) {
        let links = 0;
        for (let i of args) {
            if (parse(i).hostname) links++
        }

        if (message.embed.length > links) {
            client.emit('antiRaid', ('selfbot', message.member, message.guild))
        }
    }

    // let ticket = client.models.ticket.findOne({ id: message.guild.id })
    // if (ticket) {
    //     return;
    //     let user = client.users.cache.get(ticket.author);
    //     if (!user) return message.channel.send(
    //         new MessageEmbed()
    //             .setColor(client.colors.orange)
    //             .setDescription(`${client.signs.no} | L'utilisateur n'est plus valide ! Message non envoyé.`)
    //     );
    //
    //     try {
    //         user.send(
    //             new MessageEmbed()
    //                 .setColor(client.colors.invisible)
    //                 .setAuthor(message.author.username, message.author.avatarURL({ dynamic: true }))
    //                 .setDescription(message.content)
    //         );
    //     } catch (e) {
    //         if (e) return message.channel.send(
    //             new MessageEmbed()
    //                 .setColor(client.colors.orange)
    //                 .setDescription(`${client.signs.no} | Je ne peux pas envoyer de message a cet utilisateur, le message n'a pas été envoyé !`)
    //         );
    //     }
    //
    //     message.delete();
    //     return message.channel.send(
    //         new MessageEmbed()
    //             .setColor(client.colors.orange)
    //             .setAuthor(message.author.username, message.author.avatarURL({ dynamic: true }))
    //             .setDescription(message.content)
    //     );
    // }

    await client.functions.scanMsg(client, message);

    if (message.content.startsWith(prefix)) {

        let command = args.shift().toLocaleLowerCase();

        let sudo = false;
        if (command === 'sudo' && message.author.id === client.settings.owner) {
            sudo = true;
            command = args[0];
            args = args.slice(1)
        }

        let cmdInfo = client.commands.get(command);
        if (!cmdInfo) cmdInfo = client.aliases.get(command);

        if (cmdInfo) {

            if (!guildConfig) {
                guildConfig = await client.functions.initGuild(client, message.guild, message.channel.id);
            }

            let guildLang = guildConfig.lang || 'fr'

            let user;
            if (cmdInfo.category === "owner" && message.author.id !== client.settings.owner) return message.channel.send(client.embeds.notAllowed(client, cmdInfo.name, 'fr'))
            if (cmdInfo.category === 'moderation') {
                user = message.guild.member(message.mentions.users.first());
                if (!user) user = client.functions.findUser(args[0], message.guild);
                if (!client.functions.compareUser(message.member, user)) return message.channel.send(
                    client.embeds.userIsHigher(client, user, guildLang)
                )
            }
            if (cmdInfo.clientPerms && cmdInfo.clientPerms.length > 0) {
                let clientPerms = cmdInfo.clientPerms;
                for (let i in clientPerms) {
                    let perm = clientPerms[i];
                    if (!message.guild.member(client.user).hasPermission(perm)) return message.channel.send(client.embeds.missingPerm.client(client, perm, 'fr'))
                }
            }

            let requirement = null;
            if (cmdInfo.category) requirement = guildConfig.config[cmdInfo.category.toLocaleLowerCase()];
            if (requirement && cmdInfo.name) requirement = requirement[cmdInfo.name.toLowerCase()];
            if (requirement && !sudo) {
                switch (requirement.requirement) {
                    case "perm":
                        if (requirement.permission && !message.member.hasPermission(requirement.permission)) return message.channel.send(
                            client.embeds.missingPerm.userPerm(client, requirement.permission, guildLang)
                        );

                        break;

                    case "role":
                        if (requirement.role) {
                            let role = message.guild.roles.cache.get(requirement.role);

                            if (role && !message.member.roles.cache.has(role.id)) return message.channel.send(
                                client.embeds.missingPerm.userRole(client, role, guildLang)
                            );
                        }

                        break;
                }
            }

            let cmdLang = client.langs.get(guildLang).commands[cmdInfo.category.toLocaleLowerCase()][cmdInfo.name.toLowerCase()];

            cmdInfo.run(client, message, args, cmdLang, guildLang, guildConfig);
        }
    } else {
        if (guildConfig.xp.enabled) {
            if (xp.has(message.author.id)) return;

            let user = await client.models.userXP.findOne({ userID: message.author.id, guildID: message.guild.id });

            if (!user) user = await client.functions.initUserXP(client, message.guild, message.author);

            let currLvl = user.lvl
            let xpWin = Math.round(Math.random()*15)+10;
            let toLvlUp = Math.round(Math.exp(user.lvl)+(500/(currLvl || 1)));
            let totalXP = user.xp+xpWin

            if (totalXP >= toLvlUp) {
                user.lvl = user.lvl+1;
                user.xp = 0;

                await user.updateOne(user);

                message.channel.send(client.langs.get(guildConfig.lang || 'fr').util.lvlUp.replace('{emoji}', client.signs.tada).replace('{user}', message.author).replace('{lvl}', user.lvl))

                if (guildConfig.xp.roles[user.lvl.toString()]) {
                    let role = message.guild.roles.cache.get(guildConfig.xp.roles[user.lvl.toString()]);
                    if (!role) return;

                    message.member.roles.add(role)
                }
            } else {
                user.xp = totalXP;
                await user.updateOne(user)
            }

            xp.add(message.author.id);

            setTimeout(() => {
                xp.delete(message.author.id)
            }, 30000)
        }
    }
}