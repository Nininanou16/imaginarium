const {MessageEmbed} = require('discord.js')

module.exports = async (client, message) => {
    return;

    if (message.author.bot) return;
    console.log('dm')
    let mainGuild = client.guilds.cache.get(client.settings.mainGuildID);
    if (!mainGuild) return console.log('no guild');

    let topic;

    let ticketCategory = mainGuild.channels.cache.get(client.settings.ticketCategoryID);
    if (!ticketCategory) return console.log('no ticket');



    let ticket = await client.models.ticket.findOne({ author: message.author.id });
    if (!ticket) {
        console.log('no ticket')
        let confirm = await message.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(`Vous êtes sur le point d'ouvrir un ticket de support, voulez vous procéder ?\nTout abus sera sanctionné.`)
        )

        confirm.react('✅');

        let filter = (reaction, user) => {
            return reaction.emoji.name === '✅' && user.id === message.author.id
        }

        const collector = confirm.createReactionCollector(filter, {
            timeout: 30000
        });

        collector.on('collect', async (reaction, user) => {
            let ticketChannel = await mainGuild.channels.create(`🎟️》${message.author.tag.replace('#', '-')}`, {
                type: 'text',
                topic: topic,
                parent: ticketCategory// ,
                // permissionOverwrites: {
                //
                // }
            });

            ticket = new client.models.ticket({
                id: ticketChannel.id,
                author: message.author.id,
                subject: message.content
            }).save();

            ticketChannel.send(
                new MessageEmbed()
                    .setColor(client.colors.orange)
                    .setAuthor(message.author.username + ' - Problème', message.author.avatarURL({ dynamic: true }))
                    .setDescription(message.content)
            ).then(msg => msg.pin())
        })
    } else {
        console.log(ticket)
        let ticketChannel = client.channels.cache.get(ticket.id);
        if (!ticketChannel) {
            console.log('ticket db not exists')
            return await ticket.deleteOne();
        }

        ticketChannel.send(
            new MessageEmbed()
                .setAuthor(message.author.username + ' - Utilisateur', message.author.avatarURL({ dynamic: true }))
                .setColor(client.invisible)
        )
    }
}