module.exports = async (client, member) => {
    let invites = await member.guild.fetchInvites();
    let ei = client.invites[member.guild.id];

    let invite = invites.find(i => ei.get(i.code).uses < i.uses);
    if (invites.map(i => `${i.code}---${i.uses}`) === ei.map(i => `${i.code}---${i.uses}`) && !member.guild.features.includes('VANITY_URL') && !invite) return; // vanity invite

    let user = client.users.cache.get(invite.inviter.id);
    if (!user || user.bot) return;

    let userInvites = await client.models.userInvites.findOne({ userID: user.id, guildID: member.guild.id });
    if (!userInvites) userInvites = await client.functions.initUserInvites(client, member.guild, user);

    let fake = false;
    if (member.user.createdTimestamp > (Date.now()-172800000) || user.id === member.user.id) fake = true;

    switch (fake) {
        case true:
            userInvites.invites.fake = userInvites.invites.fake+1;
            break;

        default:
            userInvites.invites.true = userInvites.invites.true+1;
    }

    let usrInvites = userInvites.invites
    let total = usrInvites.true+usrInvites.bonus-usrInvites.fake

    await userInvites.updateOne(userInvites);

    let guildConfig = client.models.guild.findOne({ id: member.guild.id });
    if (guildConfig.invites && guildConfig.invites[total.toString()]) {
        let role = member.guild.roles.cache.get(guildConfig.invites[total.toString()]);
        if (!role) return;

        let inviteMember = member.guild.member(user);
        if (!inviteMember) return;

        inviteMember.roles.add(role)
    }
}