const {readdirSync} = require("fs");

module.exports = async (client) => {

    readdirSync('./src/Commands/').forEach(category => {
        let commands = readdirSync(`./src/Commands/${category}`).filter(file => file.endsWith('.js'));

        for (let file of commands) {
            let command = require(`../Commands/${category}/${file}`);
            command.category = category
            command.name = file.split('.')[0]

            client.commands.set(command.name, command);
            if (command.aliases && command.aliases.length > 0) command.aliases.forEach(alias => client.aliases.set(alias, command));
        }
    })

    client.signs = {
        warning: client.emojis.cache.get('836705657973506078'),
        muted: client.emojis.cache.get('837344336077127720'),
        unmuted: null,
        ban: client.emojis.cache.get('837831115568840784'),
        yes: client.emojis.cache.get('837348580414980187'),
        no: client.emojis.cache.get('837348184023367690'),
        on: client.emojis.cache.get('830579252033355777'),
        off: client.emojis.cache.get('830579525326340137'),
        loading: client.emojis.cache.get('846688961953792030'),
        tada: client.emojis.cache.get('850354851812605953'),
        plus: client.emojis.cache.get('850678737489362974'),
        minus: client.emojis.cache.get('850678737035984916'),
        exclamation: client.emojis.cache.get('850681752674631680'),
        check: client.emojis.cache.get('850681215986958337')
    };

    client.console.ready();

    client.console.info(`Connected as : ${client.user.tag}`);
    client.console.info(`Ready on : ${client.guilds.cache.size} servers`);
    client.console.info(`Listening to : ${client.users.cache.size} users`);
    client.console.info(`With ${client.commands.size} commands available`);

    client.invites = {};

    client.guilds.cache.forEach(async (g) => {
        if (!g.member(client.user).hasPermission('ADMINISTRATOR')) return;
        let invites = await g.fetchInvites();
        client.invites[g.id] = invites;
    })

    client.user.setPresence({ activity: { name: `${client.users.cache.size} utilisateurs`, type: 'WATCHING'}, status: 'idle'})

    let frenchStatus = client.channels.cache.get(client.settings.statusChannelFR);
    let englishStatus = client.channels.cache.get(client.settings.statusChannelEN);

    try {
        frenchStatus.setName('🟢》Statut : En ligne');
    } catch (e) {
        client.console.error(e)
    }

    try {
        englishStatus.setName('🟢》Status: Online');
    } catch (e) {
        client.console.error(e)
    }
}