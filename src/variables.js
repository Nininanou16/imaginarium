const {Collection} = require("discord.js");
const {readdirSync} = require('fs');

module.exports = async (client) => {

    client.commands = new Collection();
    client.aliases = new Collection();
    client.queue = new Map();

    client.music = require('./music');

    client.langs = new Map();

    client.settings = require('../settings.json');
    client.console = require('./console');

    client.colors = {
        orange: '#de941d',
        yellow: '#fee75c',
        green: '#57f287',
        cyan: '#2dc18a',
        lightblue: '#5865f2',
        blue: '#3236c7',
        purple: '#8940c7',
        pink: '#eb459e',
        red: '#ed2425',
        invisible: '#2f3136',
        invisible2: '#36393F'
    };

    client.tempChannels = new Map();

    let langDir = readdirSync('./src/langs').filter(file => file.endsWith('.json'));

    for (let lang of langDir) {
        client.langs.set(lang.split('.')[0], require(`./langs/${lang}`));
    }

    client.categories = readdirSync('./src/Commands');

    client.functions = require('./functions');

    client.embeds = require('./Embeds');
}