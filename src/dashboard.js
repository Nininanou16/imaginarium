const express = require('express');
const path = require('path');
const Discord = require('discord.js');
const passport = require('passport');
const Strategy = require('passport-discord').Strategy;

const app = express();
const dir = path.resolve(__dirname+`${path.sep}public`);

app.listen(80);
app.set('view engine', 'ejs');

module.exports = async (client) => {
    const render = (path, req, res, Params = {}) => {
        res.render(`${dir}/${path}/index.ejs`, Object.assign(Params, client), (err, html) => {
            if (err) return client.emit('error', err)
        });
    }

    client.console.info('Dashboard online')
    app.get('/', (req, res) => {
        res.send('Home');
    });
}