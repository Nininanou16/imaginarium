const ms = require('ms');
const {MessageEmbed} = require('discord.js');

module.exports = {
    aliases: ['tm', 'tmute', 'tempm'],
    description: 'Rendez un utilisateur muet de manière temporaire',
    run: async (client, msg, args, text, lang, guildConfig) => {
        let user = message.mentions.users.first();
        if (!user) user = client.functions.findUser(args, msg.guild);

        if (!user) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.settings.orange)
                .setDescription(text.noUser.replace('{emoji}', client.signs.warning).replace('{search}', args.join(' ')))
        );

        let mutedRole;

        if (!guildConfig.muteRole || !msg.guild.roles.cache.get(!guildConfig.muteRole)) {
            msg.channel.send(
                new MessageEmbed()
                    .setColor(client.invisible)
                    .setDescription(text.creatingMuteRole)
            );

            mutedRole = await msg.guild.roles.create('Muted', {reason: 'Creating Muted role'}).then(async r => {
                msg.guild.channels.cache.forEach(channel => {
                    channel.updateOverwrite(r, {
                        SEND_MESSAGES: false,
                        ADD_REACTIONS: false
                    });
                });

                await client.models.guild.findOne({ id: msg.guild.id }, { mutedRole: r.id })
            });
        } else mutedRole = msg.guild.roles.cache.get(guildConfig.muteRole);

        if (user.roles.cache.has(mutedRole.id)) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.alreadyMuted.replace('{emoji}', client.signs.warning))
        );

        let time = ms(args[1]);

        let db = new client.models.tempmute({
            user: user.id,
            timeout: Date.now()+time,
            server: msg.guild.id
        })

        await db.save(err => {
            if (err) client.emit('error', err);
        })

        if (!time) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.noTime.replace('{emoji}', cient.signs.warning))
        );

        user.roles.add(mutedRole);

        msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(text.muted.replace('{emoji}', client.signs.muted).replace('{user}', user.user.tag).replace('{time}', args[1]))
        );

        setTimeout(() => {
            user.roles.remove(mutedRole);
            msg.channel.send(
                new MessageEmbed()
                    .setColor(client.invisible)
                    .setDescription(text.unMuted.replace('{emoji}', client.sings.unmuted).replace('{user}', user.user.tag))
            )
        })
    }
}