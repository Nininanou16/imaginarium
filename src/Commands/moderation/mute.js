const {MessageEmbed} = require('discord.js')

module.exports = {
    aliases: ['mut'],
    description: 'Rendez un utilisateur muet pour une durée indéfinie',
    run: async (client, msg, args, text, lang, guildConfig) => {
        let user = message.mentions.users.first();
        if (!user) user = client.functions.findUser(args, msg.guild);

        if (!user) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.settings.orange)
                .setDescription(text.noUser.replace('{emoji}', client.signs.warning).replace('{search}', args.join(' ')))
        );

        let mutedRole;

        if (!guildConfig.muteRole || !msg.guild.roles.cache.get(!guildConfig.muteRole)) {
            msg.channel.send(
                new MessageEmbed()
                    .setColor(client.invisible)
                    .setDescription(text.creatingMuteRole)
            );

            mutedRole = await msg.guild.roles.create('Muted', {reason: 'Creating Muted role'}).then(async r => {
                msg.guild.channels.cache.forEach(channel => {
                    channel.updateOverwrite(r, {
                        SEND_MESSAGES: false,
                        ADD_REACTIONS: false
                    });
                });

                await client.models.guild.findOne({ id: msg.guild.id }, { mutedRole: r.id })
            });
        } else mutedRole = msg.guild.roles.cache.get(guildConfig.muteRole);

        if (user.roles.cache.has(mutedRole.id)) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.alreadyMuted.replace('{emoji}', client.signs.warning))
        );

        user.roles.add(mutedRole);
        msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(text.replace('{emoji}', client.signs.muted).replace('{user}', user.user.tag))
        );

        let reason = args.slice(1).join(' ') || client.langs.get(guildConfig.lang || 'fr').noReason;

        client.emit('logs', ('mute', user, msg.author))
    }
}