const {MessageEmbed} = require('discord.js')

module.exports = {
    aliases: [],
    description: '',
    usage: [],
    run: (client, msg, args, text, lang) => {
        let user = msg.guild.member(msg.mentions.users.first());
        if (!user) user = client.functions.findUser(args[0], msg.guild);

        if (!user) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.noUser.replace('{emoji}', client.signs.no))
        );

        if (user.size > 1) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.multipleUsers.replace('{emoji}', client.signs.warning))
        );

        if (!user.bannable) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.notBannable.replace('{emoji}', client.signs.warning))
        )

        user.kick(`Expulsé par ${msg.author.tag} - ${msg.author.username} | ${args.slice(1).join(' ') || 'Aucune raison'}`);

        msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(text.kicked.replace('{emoji}', client.signs.ban).replace('{user}', user.user.tag))
        )
    }
}