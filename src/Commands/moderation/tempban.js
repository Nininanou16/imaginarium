const {MessageEmbed} = require('discord.js')
const ms = require('ms')

module.exports = {
    aliases: [],
    description: '',
    usage: [],
    run: (client, msg, args, text, lang) => {
        let user = msg.guild.member(msg.mentions.users.first());
        if (!user) user = client.functions.findUser(args[0], msg.guild);

        if (!user) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.noUser.replace('{emoji}', client.signs.no))
        );

        if (user.size > 1) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.multipleUsers.replace('{emoji}', client.signs.warning))
        );

        if (!user.bannable) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.notBannable.replace('{emoji}', client.signs.warning))
        )

        let timeout = ms(args[1]);

        if (!timeout) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.timeError.replace('{emoji}', client.signs.warning))
        );

        user.send(
            new MessageEmbed()
                .setColor(client.colors.red)
                .setDescription(text.gotBanned.replace('{emoji}', client.signs.ban).replace('{time}', args[1]).replace('{guild}', msg.guild.name))
        );

        user.ban({ reason: `Banni par ${msg.author.tag} - ${msg.author.username} | ${args.slice(2).join(' ') || 'Aucune raison'}`, days: 7 });

        setTimeout(() => {
            msg.guild.members.unban(user.user.id);
            msg.channel.send(
                new MessageEmbed()
                    .setColor(client.colors.orange)
                    .setDescription(text.unbanned.replace('{emoji}', client.signs.yes).replace('{user}', user.user.tag))
            )
        }, timeout)

        msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(text.banned.replace('{emoji}', client.signs.ban).replace('{user}', user.user.tag).replace('{time'))
        )
    }
}