const { MessageEmbed } = require('discord.js')

module.exports = {
    aliases: ['h'],
    run: (client, msg, args) => {
        if (!args[0]) {
            let embed = new MessageEmbed()
                .setColor(client.colors.invisible);

            client.categories.forEach(category => {
                let txt = [];
                let count = 0;

                client.commands.filter(cmd => cmd.category === category).forEach(command => {
                    count++;
                    txt.push(`\`${command.name}\``)
                });

                embed.addField(
                    `${category.toLowerCase() === 'owner' ? ':crown:' :
                            category.toLowerCase() === 'info' ? ':information_source:' :
                            ':question:'} ❯ ${category.slice(0, 1).toUpperCase()+category.slice(1)} (${txt.length}) :`,
                    txt.join(' · ')
                )
            })

            msg.channel.send(embed);
        } else {
            let command = client.commands.get(args[0]);
            if (!command) command = client.aliases.get(args[0]);

            if (!command) return msg.channel.send(
                new MessageEmbed()
                    .setColor(client.colors.red)
                    .setDescription(`:x: | Je n'ai trouvé aucune commande \`${args[0]}\`, pour voir la liste des commandes faites \`${client.settings.prefix}help\``)
                    .setFooter(msg.author.username, msg.author.avatarURL({}))
                    .setTimestamp()
            );

            msg.channel.send(
                new MessageEmbed()
                    .setColor(client.colors.lightblue)
                    .setTitle(`Commande \`${client.settings.prefix+args[0]}\``)
                    .setDescription(`**Description** :\n${command.description || 'Aucune'}\n\n**Aliases** :\n${command.aliases.join(' - ') || 'Aucun'}\n\nExemples d'utilisation :\n\`${command.usage.join('\`\n\`') || 'Aucun exemples'}\``)
            )
        }
    }
}