const {MessageEmbed} = require('discord.js');

module.exports = {
    run: async (client, msg, args, text) => {
        let userInvites = await client.models.userInvites.findOne({ userID: msg.author.id, guildID: msg.guild.id });
        if (!userInvites) userInvites = await client.functions.initUserInvites(client, msg.guild, msg.author);
        console.log(userInvites)
        userInvites = userInvites.invites
        let total = userInvites.true+userInvites.bonus-userInvites.fake

        msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(`Invitations de **${msg.member.nickname || msg.author.username}** :\n
                ${client.signs.plus} | **${userInvites.true}** Vraies
                ${client.signs.minus} | **${userInvites.fake}** Fausses
                ${client.signs.exclamation} | **${userInvites.bonus}** Bonus
                ${client.signs.check} | **${total}** Total`)
        )
    }
}