const moment = require('moment');
const {MessageEmbed} = require('discord.js');
const {getdetails} = require('api-npm');

module.exports = {
    aliases: ['node'],
    description: 'Obtenez des informations concernant vos paquets npm favorits !',
    usage: ['npm [package name]'],
    run: (client, msg, args, text, lang) => {
        let name = args.join(' ');
        if (!name || name.length < 1) return msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.noName.replace('{emoji}', client.signs.no))
        );

        getdetails(name, (data) => {
            if (!data || data.length < 1) return msg.channel.send(
                new MessageEmbed()
                    .setColor(client.colors.orange)
                    .setDescription(text.noModule.replace('{emoji}', client.signs.no).replace('{search}', name))
            );

            let embed = new MessageEmbed()
                .setThumbnail('https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Npm-logo.svg/1200px-Npm-logo.svg.png')
                .setColor(client.colors.red)

            console.log(data.repository)

            if (data.name) embed.setTitle(data.name);
            if (data.description) embed.setDescription(data.description);
            if (data.repository.url && data.repository.type === 'git' && !data.repository.url.includes('ssh')) embed.setURL(data.repository.url.substring(4));
            if (data.time && data.time.modified && data.time.created) embed.addField(text.dates, `${text.creation.replace('{date}', moment(data.time.created).format('DD/MM/YYYY'))}\n${text.modification.replace('{date}', moment(data.time.modified).format('DD/MM/YYYY'))}`, true);
            if (data.keywords && data.keywords.length > 0) embed.addField(text.keywords, `\`${data.keywords.join('`, `')}\``, true);
            // if (data.maintainers && data.maintainers[0] && data.maintainers[0].name && data.maintainers[0].email) embed.addField(text.author, `[${data.maintainers[0].name}](mailto:${data.maintainers[0].email})`)

            msg.channel.send(embed)
        })
    }
}