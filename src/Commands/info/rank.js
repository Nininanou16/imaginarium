const {MessageEmbed, MessageAttachment} = require('discord.js')
const Canvas = require('canvas');

module.exports = {
    run: async (client, msg, args, text) => {
        if (args[0] === 'settings') {

        } else {
            try {
                msg.channel.startTyping()
                let user = await client.models.userXP.findOne({ userID: msg.author.id, guildID: msg.guild.id });
                let users = await client.models.userXP.find({ guildID: msg.guild.id });
                users = users.sort((a, b) => {
                    if (a.lvl > b.lvl) return -1;
                    if (a.lvl === b.lvl && a.xp >= b.xp) return -1;
                    return 1;
                });

                function format(text) {
                    text = text.toString()
                    let count = parseInt(text);
                    let num = text.split('');
                    if (3 >= num.length) return text;
                    if (6 >= num.length) return ((count/1000).toFixed(2)).toString()+'k';
                    return ((count/1000000).toFixed(2)).toString()+'M'
                }

                let rank;
                let lvl = user.lvl;
                let xp = user.xp;
                let toNext = Math.round(Math.exp(lvl)+(500/(lvl || 1)));

                for (let i in users) {
                    if (user.id === users[i].id) rank = parseInt(i)+1;
                }

                const canvas = Canvas.createCanvas(1000, 300);
                let ctx = canvas.getContext('2d')
                Canvas.registerFont('./assets/Discord/Light.ttf', { family: 'Cubest', weight: 'normal', style: 'normal' });
                Canvas.registerFont('./assets/Discord/Medium.ttf', { family: 'Cubest', weight: 'bold', style: 'bold' });

                let avatar = await Canvas.loadImage(msg.author.displayAvatarURL({ format: 'png', size: 128 }))
                let username = msg.author.username;
                if (username.length > 12) username = username.substr(0, 12)+'...'

                // fond
                ctx.fillStyle = '#212529'
                ctx.fillRect(0, 0, 1000, 300);

                // pseudo
                ctx.font = 'bold 32px Cubest';
                ctx.fillStyle = '#ffffff';
                const left = ctx.measureText(username).width;
                ctx.textAlign = 'start'
                ctx.fillText(username, 260, 130)

                // tag
                ctx.font = `28px Cubest`;
                ctx.fillStyle = '#aaaaaa';
                ctx.textAlign = 'center';
                ctx.fillText(`#${msg.author.discriminator}`, left+330, 130);

                // lvl
                ctx.font = 'bold 24px Cubest';
                ctx.fillStyle = '#ccc';
                lvl = text.lvl+lvl
                let lvlLength = ctx.measureText(lvl).width
                ctx.fillText(lvl, 1000-lvlLength, 50)

                // xp
                ctx.font = 'bold 22px Cubest';
                ctx.fillStyle = '#ccc';
                let txt = `${format(xp)}/${format(toNext)}`;
                ctx.fillText(txt, 1000-ctx.measureText(txt).width, 130)

                // rank
                ctx.font = 'bold 22px Cubest';
                ctx.fillStyle = '#ccc';
                rank = text.rank+rank;
                let rankLength = ctx.measureText(rank).width
                ctx.fillText(rank, 950-lvlLength-rankLength, 50)

                // progression bg
                ctx.fillStyle = '#777';
                ctx.beginPath()
                ctx.arc(280, 200, 18.5, Math.PI*1.5, Math.PI*0.5, true);
                ctx.fill();
                ctx.fillRect(280, 181.5, 640, 37.5);
                ctx.arc(920, 200, 18.75, Math.PI*1.5, Math.PI*0.5, false);
                ctx.fill();

                // progression
                let progress = Math.round((xp*640)/toNext);
                ctx.beginPath()
                ctx.fillStyle = '#fff';
                ctx.arc(280, 200, 18.5, Math.PI*1.5, Math.PI*0.5, true);
                ctx.fill();
                ctx.fillRect(280, 181.5, progress, 37.5);
                ctx.arc(280+progress, 200, 18.75, Math.PI*1.5, Math.PI*0.5, false);
                ctx.fill();

                // pdp
                ctx.beginPath();
                ctx.arc(135, 145, 100, 0, Math.PI*2, true);
                ctx.closePath();
                ctx.clip();

                ctx.drawImage(avatar, 35, 45, 200, 200);
                ctx.restore();

                let atta = new MessageAttachment(canvas.toBuffer(), 'rank.png');

                msg.channel.send(atta)
                msg.channel.stopTyping()
            } catch (e) {
                if (e) {
                    msg.channel.stopTyping();
                }
            }
        }
    }
}