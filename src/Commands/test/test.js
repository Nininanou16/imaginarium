module.exports = {
    run: async (client, msg, args, text, lang) => {
        let guild = await client.models.guild.findOne({ id: msg.guild.id });

        await guild.updateOne({
            antiRaid: {
                antiLink: true,
                antiLinkWL: [],
                antiInvite: true,
                antiInviteWL: []
            }
        })

        let updateGuild = await client.models.guild.findOne({ id: msg.guild.id });
        msg.channel.send(`\`\`\`json\n${updateGuild}\`\`\``)
    }
}