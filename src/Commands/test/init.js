const {define} = require('sequelize')

module.exports = {
    aliases: [],
    description: '',
    usage: [],
    run: async (client, msg, args, text, lang) => {
        try {
            let guild = client.models.guild.build(
                {
                id: msg.guild.id,
                lang: 'fr',
                logs: {
                    voice: {
                        join: true,
                        leave: true,
                        create: true,
                        delete: true,
                        selfMute: true,
                        selfDeafen: true,
                        serverMute: true,
                        serverDeafen: true,
                        move: true,
                        afkChannel: true
                    },
                    voiceChannel: 800866024839970901,
                    server: {
                        join: true,
                        leave: true,
                        createChannel: true,
                        deleteChannel: true,
                        boost: true,
                        unboost: true,
                        createRole: true,
                        deleteRole: true,
                        nameEdit: true,
                        avatarEdit: true,
                        vanityURLEdit: true,
                        ownerEdit: true
                    },
                    serverChannel: 800866024839970901,
                    member: {
                        obtainRole: true,
                        looseRole: true,
                        ban: true,
                        kick: true,
                        avatarEdit: true,
                        usernameEdit: true,
                        nicknameEdit: true,
                        statusEdit: true
                    },
                    memberChannel: 800866024839970901,
                    antiRaid: {
                        antiSpam: true,
                        antiSelfBot: true,
                        antiToken: true,
                        antiWebHook: true,
                        antiChannelDelete: true,
                        antiChannelCreate: true,
                        antiEveryone: true,
                        antiMassBan: true,
                        antiMassKick: true,
                        captchaPass: true,
                        captchaFail: true
                    },
                    antiRaidChannel: 800866024839970901
                },
                config: {
                    moderation: {
                        kick: { requirement: 'perm', permission: 'KICK_MEMBERS', role: null },
                        ban: { requirement: 'perm', permission: 'BAN_MEMBERS', role: null },
                        tempban: { requirement: 'perm', permission: 'BAN_MEMBERS', role: null },
                        unban: { requirement: 'perm', permission: 'BAN_MEMBERS', role: null },
                        mute: { requirement: 'perm', permission: 'MANAGE_MESSAGES', role: null },
                        tempMute: { requirement: 'perm', permission: 'MANAGE_MESSAGES', role: null },
                        unmute: { requirement: 'perm', permission: 'MANAGE_MESSAGES', role: null },
                        clear: { requirement: 'perm', permission: 'MANAGE_MESSAGES', role: null }
                    }
                }
            })

            await guild.save()

            msg.channel.send(text)
        } catch (e) {
            if (e) {
                console.log(e)
            }
        }

    }
}

// salon général : 800866024839970901