const { MessageEmbed } = require('discord.js')
const inspect = require('object-inspect');
const axios = require('axios')

module.exports = {
    name: 'eval',
    aliases: ['e'],
    category: "utility",
    description: 'Evaluate code',
    run: async (client, message, args) => {

        let msg = await message.channel.send(new MessageEmbed().setColor(client.invisible).setDescription('Évaluation du code en cours...'))

        try {
            let evaluated = await eval(args.join(' '));
            let code = await inspect(evaluated);

            if (code.toString().length >= 1024) {
                let hastebin = await axios.post('https://hastebin.com/documents', code.toString());
                let link = hastebin.data.key;

                msg.delete();

                return message.channel.send(
                    new MessageEmbed()
                        .setColor(client.colors.orange)
                        .addField('📥 | Entrée de code :', `\`\`\`js\n${args.join(' ')}\`\`\``)
                        .addField(`📤 | Sortie de code : (0-1000)`, `https://hastebin.com/${link}.js\n\`\`\`js\n${code.toString().substring(0, 900).split(',').join(',\n')}\`\`\``),
                    // new MessageAttachment(JSON.stringify(code).toString(), 'eval.json')
                )
            } else {
                message.channel.send(
                    new MessageEmbed()
                        .setColor(client.colors.green)
                        .addField('📥 | Entrée de code :', `\`\`\`js\n${args.join(' ')}\`\`\``)
                        .addField('📤 | Sortie de code :', `\`\`\`js\n${code.toString().split(',').join(',\n')}\`\`\``)
                )

                msg.delete();
            }
        } catch (e) {
            msg.delete();
            console.log(e)
            message.channel.send(new MessageEmbed().setColor(client.colors.red).setTitle('Erreur de l\'évaluation :').setDescription(`\`\`\`js\n${e}\`\`\``))
        }
    }
}