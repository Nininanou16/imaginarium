module.export = {
    run: async (client, message, args, text, lang) => {
        let guild = await client.models.guild.findOne({ id: message.guild.id });
        guild.prefix = args[0];
        await guild.updateOne(guild)
    }
}