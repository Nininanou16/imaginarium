const {MessageEmbed} = require('discord.js');

module.exports = {
    aliases: ['puissace4', 'p4'],
    description: '',
    usage: [],
    run: async (client, msg, args, text, lang) => {
        let table = [
            ['⬛','⬛','⬛','⬛','⬛','⬛','⬛'],
            ['⬛','⬛','⬛','⬛','⬛','⬛','⬛'],
            ['⬛','⬛','⬛','⬛','⬛','⬛','⬛'],
            ['⬛','⬛','⬛','⬛','⬛','⬛','⬛'],
            ['⬛','⬛','⬛','⬛','⬛','⬛','⬛'],
            ['⬛','⬛','⬛','⬛','⬛','⬛','⬛']
        ];
        let empty = '⬛';
        let red = '🟥';
        let blue = '🟦';

        let opponent = msg.mentions.users.first();
        if (!opponent) return;

        table[0][0] = blue;
        let turn = msg.author;

        let embed = new MessageEmbed()
            .setTitle(`Puissance 4\n${msg.author.username} VS ${opponent.username}\nTour de ${msg.author.username}`)
            .setColor(client.colors.invisible)
            .setDescription(formatTable())

        let game = await msg.channel.send(embed);

        msg.author.color = 'blue';
        msg.author.piece = blue;

        opponent.color = 'red';
        opponent.piece = red;

        let menuEmojis = ['⬅', '➡', '✅'];

        for (let i in menuEmojis) {
            await game.react(menuEmojis[i])
        }

        let filter = (reaction, user) => {
            return menuEmojis.includes(reaction.emoji.name) && (user.id === msg.author.id || user.id === opponent.id)
        }

        const collector = game.createReactionCollector(filter, { timeout: 1800000 });

        collector.on('end', () => {
            msg.channel.send('fin collector')
        })

        let place = [0,0]

        collector.on('collect', async (reaction, user) => {
            await reaction.users.remove(user)
            if (user.id === turn.id) {
                switch (reaction.emoji.name) {
                    case '⬅':
                        goRight()
                        break;

                    case '➡':
                        goLeft()
                        break;

                    case '✅':
                        for (let i = 0; i < table.length; i++) {
                            if (table[i][place[0]] === empty) place[1] = place[1]+1
                        }
                        table[place[1]][place[0]] = turn.piece;
                        let old;
                        if (turn.id === msg.author.id) {
                            turn = opponent
                            old = msg.author
                        }
                        else {
                            turn = msg.author
                            old = opponent
                        }

                        table[0][0] = turn.piece;
                        update(place, [0, place[0]], old, true);
                        break;
                }
            }
        })

        function goLeft() {
            if (place[0] === 6) {
                place[0] = 0
                if (table[place[1]][place[0]] !== empty) return goLeft()
                return update(place, [0, 6])
            } else {
                place[0] = place[0]+1
                let oldPlace = [0, place[0]-1]
                if (table[place[1]][place[0]] !== empty) return goLeft()
                return update(place, oldPlace, turn)
            }
        }

        function goRight() {
            if (place[0] === 0) {
                place[0] = 6
                if (table[place[1]][place[0]] !== empty) return goRight()
                return update(place, [0, 0])
            } else {
                place[0] = place[0]-1
                let oldPlace = [0, place[0]+1]
                if (table[place[1]][place[0]] !== empty) return goRight()
                return update(place, oldPlace, turn)
            }
        }

        function update(newPlace, oldPlace, user, validate) {
            table[newPlace[1]][newPlace[0]] = user.piece;
            if (!(validate && newPlace[1] === 0)) {
                table[oldPlace[0]][oldPlace[1]] = empty;
            }

            if (validate) {
                if (checkWin()) {
                    switch(checkWin()) {
                        case 'blue':
                            return game.edit(
                                embed.setTitle(text.win.replace('{user}', msg.author.username).replace('{user2}', opponent.username))
                            );
                            break;

                        case 'red':
                            return game.edit(
                                embed.setTitle(text.win.replace('{user}', opponent.username).replace('{user2}', msg.author.username))
                            );
                            break;
                    }
                }
                place = [0, 0];
            }

            embed.setDescription(formatTable()).setTitle(`Puissance 4\n${msg.author.username} VS ${opponent.username}\nTour de ${turn.username}`)
            game.edit(embed)
        }

        function formatTable() {
            return table.join('\n').split(',').join('');
        }

        function checkWin() {
            let currPlace = table[place[1]][place[0]];
            if (currPlace === empty) return false;

            let alignedBlue = 0;
            let alignedRed = 0;

            for (let i = 0; i < 2; i++) {
                switch (i) {
                    // horizontal droite :
                    case 0:
                        for (let y = 0; y < 2; y++) {
                            if (table[place[1]] && table[place[1]][place[0]+y]) {
                                if (table[place[1]][place[0]+y] === blue) alignedBlue++
                                else if (table[place[1]][place[0]+y] === red) alignedRed++
                                else {
                                    alignedBlue = 0;
                                    alignedRed = 0;
                                }

                                if (alignedBlue === 2) return 'blue';
                                if (alignedRed === 2) return 'red';
                            } else if (table[place[1]] && table[place[1]][place[0]-y]) {
                                if (table[place[1]][place[0]-y] === blue) alignedBlue++
                                else if (table[place[1]][place[0]-y] === red) alignedRed++
                                else {
                                    alignedBlue = 0;
                                    alignedRed = 0;
                                }
                            }

                            console.log('horizontal')
                            console.log(alignedBlue);
                            console.log(alignedBlue);
                            if (alignedBlue === 2) return 'blue';
                            if (alignedRed === 2) return 'red';
                        }

                        alignedRed = 0;
                        alignedBlue = 0;
                        break;

                    // diagonal en bas droite :
                    case 1:
                        for (let y = 0; y < 2; y++) {
                            if (table[place[1]+y] && table[place[1]+y][place[0]+y]) {
                                if (table[place[1]+y][place[0]+y] === blue) alignedBlue++
                                else if (table[place[1]+y][place[0]+y] === red) alignedRed++
                                else {
                                    alignedBlue = 0;
                                    alignedRed = 0;
                                }

                                if (alignedBlue === 2) return 'blue';
                                if (alignedRed === 2) return 'red';
                            } else if (table[place[1]-y] && table[place[1]-y][place[0]-y]) {
                                if (table[place[1]-y][place[0]-y] === blue) alignedBlue++
                                else if (table[place[1]-y][place[0]-y] === red) alignedRed++
                                else {
                                    alignedBlue = 0;
                                    alignedRed = 0;
                                }
                            }

                            console.log('diagonal')
                            console.log(alignedBlue);
                            console.log(alignedBlue);

                            if (alignedBlue === 2) return 'blue';
                            if (alignedRed === 2) return 'red';
                        }

                        alignedRed = 0;
                        alignedBlue = 0;
                        break;

                    // vertical bas :
                    case 2:
                        for (let y = 0; y < 2; y++) {
                            if (table[place[1]+y] && table[place[1]+y][place[0]]) {
                                if (table[place[1]+y][place[0]] === blue) alignedBlue++
                                else if (table[place[1]+y][place[0]] === red) alignedRed++;
                                else {
                                    alignedBlue = 0;
                                    alignedRed = 0;
                                }

                                if (alignedBlue === 2) return 'blue';
                                if (alignedRed === 2) return 'red';
                            } else if (table[place[1]-y] && table[place[1]-y][place[0]]) {
                                if (table[place[1]-y][place[0]] === blue) alignedBlue++
                                else if (table[place[1]-y][place[0]] === red) alignedRed++;
                                else {
                                    alignedBlue = 0;
                                    alignedRed = 0;
                                }
                            }

                            console.log('vertical')
                            console.log(alignedBlue);
                            console.log(alignedBlue);

                            if (alignedBlue === 2) return 'blue';
                            if (alignedRed === 2) return 'red';
                        }

                        alignedRed = 0;
                        alignedBlue = 0;
                        break;
                }
            }

            return false;
        }
    }
}