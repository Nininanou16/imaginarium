const { MessageEmbed } = require('discord.js');

module.exports = {
    aliases: ['rpc','pfc'],
    description: 'Jouez au jeu de Pierre-Feuille-Ciseaux dans discord !',
    usage: ['rockpapaerscissors [@mention]'],
    run: async (client, message, args, text) => {
        let choices = ['\uD83C\uDFD4', '\uD83D\uDDDE', '✂']
        if (!message.mentions.users.first()) {
            let menu = await message.channel.send('',
                new MessageEmbed()
                    .setColor(client.colors.invisible)
                    .setDescription(text.choice)
            );

            for (let i in choices) {
                menu.react(choices[i])
            }

            let filter = (reaction, user) => {
                return choices.includes(reaction.emoji.name) && user == message.author
            }

            let collector = menu.createReactionCollector(filter, { timeout: 30000 });
            collector.on('collect', (reaction, user) => {
                collector.stop();

                let userChoice = reaction.emoji.name
                let botChoice = choices[Math.floor(Math.random()*choices.length)];

                if (userChoice === botChoice) {
                    return message.channel.send(
                        new MessageEmbed()
                            .setColor(client.colors.orange)
                            .setDescription(text.solo.equal.replace('{emoji}', ':tada:').replace('{userChoice}', userChoice).replace('{botChoice}', botChoice))
                    )
                }

                if (oneWin(userChoice, botChoice)) {
                    message.channel.send(
                        new MessageEmbed()
                            .setColor(client.colors.green)
                            .setDescription(text.solo.userWin.replace('{emoji}', client.signs.yes).replace('{userChoice}', userChoice).replace('{botChoice}', botChoice))
                    )
                } else {
                    message.channel.send(
                        new MessageEmbed()
                            .setColor(client.colors.red)
                            .setDescription(text.solo.botWin.replace('{emoji}', client.signs.no).replace('{userChoice}', userChoice).replace('{botChoice}', botChoice))
                    )
                }
            })
        } else {
            let accept = 0;

            let challenged = message.mentions.users.first();

            if (challenged.bot) {
                return message.channel.send(
                    new MessageEmbed()
                        .setColor(client.colors.orange)
                        .setDescription(text.noBot.replace('{emoji]', client.signs.no))
                )
            }

            let confirm = await message.channel.send('',
                new MessageEmbed()
                    .setColor(client.colors.orange)
                    .setDescription(text.challenge.replace('{emoji}', client.signs.warning).replace('{user}', message.author).replace('{validate}', client.signs.yes))
            );

            confirm.react(client.signs.yes)

            let validated = []

            const filter = (reaction, user) => {
                return (user === message.author || user === challenged) && reaction.emoji === client.signs.yes && !validated.includes(user)
            };

            const collector = await confirm.createReactionCollector(filter, { timeout: 30000 });
            collector.on('collect', async (reaction, user) => {
                validated.push(user)
                accept++


                if (accept === 2) {
                    message.channel.send(
                        new MessageEmbed()
                            .setColor(client.colors.green)
                            .setDescription(text.starting.replace('{emoji}', client.signs.warning))
                    );

                    let users = [message.author, challenged];
                    let recieved = 0;
                    let choice1;
                    let choice2;

                    for (let i in users) {
                        let user = users[i];
                        let choice;

                        try {
                            choice = await user.send(
                                new MessageEmbed()
                                    .setColor(client.colors.invisible)
                                    .setDescription(text.choice)
                            );
                        } catch (e) {
                            if (e) message.channel.send(
                                new MessageEmbed()
                                    .setColor(client.colors.red)
                                    .setDescription(text.dmError.replace('{emoji}', client.signs.no).replace('{user}', user))
                            )
                        }

                        for (let ii in choices) {
                            choice.react(choices[ii])
                        }

                        let choiceFilter = (reaction, usr) => {
                            return usr.id === user.id && choices.includes(reaction.emoji.name)
                        };

                        let choiceCollector = choice.createReactionCollector(choiceFilter, {timeout: 30000});

                        choiceCollector.on('collect', (reaction) => {
                            choiceCollector.stop()
                            recieved++
                            console.log(i)
                            if (i == 0) choice1 = reaction.emoji
                            if (i == 1) choice2 = reaction.emoji

                            if (recieved === 2) {
                                if (choice1 === choice2) {
                                    return message.channel.send(
                                        new MessageEmbed()
                                            .setColor(client.colors.invisible)
                                            .setDescription(text.equal + `\n\n${
                                                text.userChoice.replace('{user}', message.author).replace('{choice}', choice1)
                                            }\n${
                                                text.userChoice.replace('{user}', challenged).replace('{choice}', choice2)
                                            }`)
                                    )
                                }
                                if (oneWin(choice1, choice2)) {
                                    message.channel.send(
                                        new MessageEmbed()
                                            .setColor(client.colors.invisible)
                                            .setDescription(text.win.replace('{winner}', message.author).replace('{lost}', challenged))
                                    )
                                } else {
                                    message.channel.send(
                                        new MessageEmbed()
                                            .setColor(client.colors.invisible)
                                            .setDescription(text.win.replace('{winner}', challenged).replace('{lost}', message.author) + `\n\n${
                                                text.userChoice.replace('{user}', message.author.username).replace('{choice}', choice1)
                                            }\n${
                                                text.userChoice.replace('{user}', challenged.username).replace('{choice}', choice2)
                                            }`)
                                    )
                                }
                            }
                        });
                    }
                }
            })
        }
    }
}

function oneWin(one, two) {
    return (one === '\uD83C\uDFD4' && two === '✂') || (one === '\uD83D\uDDDE' && two === '\uD83C\uDFD4') || (one === '✂' && two === '\uD83D\uDDDE')
}