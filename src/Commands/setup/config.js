const {MessageEmbed} = require('discord.js')

module.exports = {
    aliases: ['conf', 'setup'],
    run: async (client, message, args, text, lang) => {
        let current = 0;
        let currentDrop = 0;

        let grid = [
            {
                high: true,
                name: 'Commandes',
                contain: []
            },
            {
                high: false,
                name: 'Logs',
                contain: []
            },
            {
                high: false,
                name: 'Utilitaire',
                contain: [
                    {
                        name: 'Prefix',
                        high: true
                    },
                    {
                        name: 'XP',
                        high: false
                    },
                    {
                        name: 'Invites',
                        high: false
                    }
                ]
            },
            {
                high: false,
                name: 'Anti-raid',
                contain: [
                    {
                        name: 'Anti Spam',
                        high: true
                    },
                    {
                        name: 'Anti SelfBot',
                        high: false
                    },
                    {
                        name: 'Anti Token',
                        high: false
                    },
                    {
                        name: 'Anti Webhook',
                        high: false
                    },
                    {
                        name: 'Anti Link',
                        high: false
                    },
                    {
                        name: 'Anti Invite',
                        high: false
                    },
                    {
                        name: 'Anti Channel Delete',
                        high: false
                    },
                    {
                        name: 'Anti Channel Create',
                        high: false
                    },
                    {
                        name: 'Anti Everyone',
                        high: false
                    },
                    {
                        name: 'Anti Mass Ban',
                        high: false
                    },
                    {
                        name: 'Anti Mass Kick',
                        high: false
                    },
                    {
                        name: 'Captcha',
                        high: false
                    }
                ]
            }
        ];

        for (let i in client.categories) {
            let high = false;
            let name = client.categories[i];
            let first = name.charAt(0).toUpperCase();
            name = first+name.substring(1);
            if (i === '0') high = true
            grid[0].contain.push({
                high,
                name
            })
        }

        let base = '';

        for (let i of grid) {
            if (i.high) base += `${i.drop ? '🔽' : '▶'} **__${i.name}__**\n\n`
            else base += `${i.drop ? '🔽' : '▶'} \`${i.name}\`\n\n`
       }

        const menu = await message.channel.send(new MessageEmbed().setColor(client.colors.invisible).setDescription(base));

        await menu.react('⏫');
        await menu.react('⏬');
        await menu.react('✅');
        await menu.react('⤴');

        function updateGrid() {
            for (let i of grid) {
                if (i.high) i.high = false;
            }
            grid[current].high = true;
            let txt = '';
            for (let i of grid) {
                if (i.high) txt += `${i.drop ? '🔽' : '▶'} **__${i.name}__**\n\n`
                else txt += `${i.drop ? '🔽' : '▶'} \`${i.name}\`\n\n`
            }

            let embed = new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(txt);

            menu.edit(embed)
        }

        function open() {
            for (let i = 0; i < grid.length; i++) {
                if (grid[i].drop) {
                    grid[i].drop = false
                }
            }
            grid[current].drop = true;
            let txt = '';

            for (let i = 0; i < grid.length; i++) {
                if (grid[i].drop) {
                    let dropdown = '';
                    for (let drop of grid[i].contain) {
                        dropdown += `${drop.high ? `**__${drop.name}__**` : `\`${drop.name}\``}\n`
                    }
                    txt += `${
                        grid[i].high ? `${grid[i].drop ? '🔽' : '▶'} **__${grid[i].name}__**`
                            : `${grid[i].drop ?  '🔽' : '▶'} \`${grid[i].name}\``
                        }\n${dropdown}`
                } else {
                    if (grid[i].high) txt += `${grid[i].drop ? '🔽' : '▶'} **__${grid[i].name}__**\n\n`
                    else txt += `${grid[i].drop ? '🔽' : '▶'} \`${grid[i].name}\`\n\n`
                }
            }

            menu.edit(
                new MessageEmbed()
                    .setColor(client.colors.invisible)
                    .setDescription(txt)
            );
        }

        function updateMenu() {
            let txt = '';
            for (let i = 0; i < grid.length; i++) {
                if (grid[i].drop) {
                    let dropdown = '';
                    for (let a = 0; a < grid[i].contain.length; a++) {
                        grid[i].contain[a].high = false;
                    }

                    grid[i].contain[currentDrop].high = true;
                    for (let drop of grid[i].contain) {
                        dropdown += `${drop.high ? `**__${drop.name}__**` : `\`${drop.name}\``}\n`
                    }

                    txt += `${grid[i].high ? `${grid[i].drop ? '🔽' : '▶'} **__${grid[i].name}__**` : `${grid[i].drop ? '🔽' : '▶'} \`${grid[i].name}\``}\n${dropdown}\n`
                } else {
                    if (grid[i].high) txt += `${grid[i].drop ? '🔽' : '▶'} **__${grid[i].name}__**\n\n`
                    else txt += `${grid[i].drop ? '🔽' : '▶'} \`${grid[i].name}\`\n\n`
                }
            }

            menu.edit(
                new MessageEmbed()
                    .setColor(client.colors.invisible)
                    .setDescription(txt)
            );
        }

        const filter = (reaction, user) => {
            return user.id === message.author.id
        }

        const collector = menu.createReactionCollector(filter, { time: 120000 });

        collector.on('collect', async (reaction, user) => {
            await reaction.users.remove(user);

            let dropped = false;
            let droppedPos = null;

            for (let i = 0; i < grid.length; i++) {
                if (grid[i].drop) {
                    dropped = true;
                    droppedPos = i;
                }
            }

            switch (reaction.emoji.name) {
                case '⏫':
                    if (dropped) {
                        if (droppedPos != null) {
                            if (currentDrop === 0) {
                                currentDrop = grid[droppedPos].contain.length-1;
                                return updateMenu();
                            }
                            currentDrop--;
                            updateMenu();
                        }
                    } else {
                        if (current === 0) {
                            current = grid.length-1;
                            return updateGrid();
                        }
                        current--
                        updateGrid();
                    }
                    break;

                case '⏬':
                    if (dropped) {
                        if (droppedPos != null) {
                            if (currentDrop === grid[droppedPos].contain.length-1) {
                                currentDrop = 0;
                                return updateMenu();
                            }
                            currentDrop++;
                            updateMenu();
                        }
                    } else {
                        if (current === grid.length-1) {
                            current = 0;
                            return updateGrid();
                        }
                        current++
                        updateGrid();
                    }
                    break;

                case '✅':
                    if (dropped) {
                        collector.stop();
                        let highlighted = null;
                        let index = 0;
                        for (let i = 0; i < grid[droppedPos].contain.length; i++) {
                            if (grid[droppedPos].contain[i].high) {
                                highlighted = true;
                                index = i;
                            }
                        }

                        const file = require(`../../configs/${grid[droppedPos].contain[index].name.toLowerCase().split(' ').join('-')}`);

                        if (!file) return;
                        await menu.edit(
                            new MessageEmbed()
                                .setColor(client.colors.orange)
                                .setDescription(text.loading.replace('{emoji}', client.signs.loading))
                        );
                        setTimeout(async () => {
                            await reaction.message.reactions.removeAll();
                            file(client, message, text, menu)
                        }, 2000)

                        return;
                    } else {
                        return open();
                    }
                    break;

                case '⤴':
                    if (dropped) {
                        grid[droppedPos].drop = false;
                        updateMenu();
                    }
                    break;
            }
        })
    }
}