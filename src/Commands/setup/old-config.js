const {MessageEmbed} = require('discord.js');

module.exports = {
    // aliases: ['conf', 'setup', 'init'],
    run: async (client, msg, args, text, lang, guildConfig) => {
        let moderationSettings = guildConfig.config.moderation
        let baseMenu = {
            Commands: {
                Games: {

                },
                Info: {

                },
                Moderation: moderationSettings,
                Setup: {

                },
                open: false
            },
            Logs: {
                open: false
            }
        };

        let menu = baseMenu;

        // menu = Object.create(menu);

        let length = Object.keys(menu).length

        // console.log(Object.keys(menu).length)

        let txt;
        let curr = 0;

        update(true)

        let navigate = await msg.channel.send(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(txt)
        );

        function update(first) {
            txt = '';
            let count = 0;
            for (let i in baseMenu) {
                if (baseMenu[i].open) {
                    txt += `\n🔽  **${i}**`
                    for (let y in menu) {
                        if (y !== 'open') {
                            txt += `\n⚙️ ${y}`
                        }
                    }
                } else if (count === curr) {
                    txt += `\n▶ ️**${i}** (${Object.keys(baseMenu[i]).length-1} options)`
                } else {
                    txt += `\n▶️ ${i}`
                }

                count++
            }
            if (!first) {
                navigate.edit(
                    new MessageEmbed()
                        .setColor(client.colors.invisible)
                        .setDescription(txt)
                )
            }
        }

        let reactions = ['⏫', '⏬', '✅', '⤴']

        navigate.react('⏫');
        navigate.react('⏬');
        navigate.react('✅');
        navigate.react('⤴');

        const filter = (reaction, user) => {
            return user.id === msg.author.id && reactions.includes(reaction.emoji.name)
        };

        const collector = navigate.createReactionCollector(filter, { timeout: 30000 });

        collector.on('collect', async (reaction, user) => {
            await reaction.users.remove(user)
            let count = 0;
            switch (reaction.emoji.name) {
                case '⏫':
                    curr--;
                    if (curr < 0) curr = (length-1)
                    update()
                    break;

                case '⏬':
                    curr++;
                    if (curr+1>length) curr = 0
                    update()
                    break;

                case '✅':
                    for (let i in baseMenu) {
                        if (count === curr) {
                            baseMenu[i].open = true;
                            menu = baseMenu[i]
                        }
                        count++
                    }
                    update();
                    break;

                case '⤴':
                    for (let i in menu) {
                        if (count === curr) {
                            menu[i].open = false
                        }
                        count++
                    }
                    menu = baseMenu
                    update();
                    break;
            }
        })
    }
}