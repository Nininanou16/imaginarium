const {MessageEmbed} = require('discord.js')

module.exports = async (client, message, text, menu) => {
    let commands = [];
    let guildConfig = client.models.guild.findOne({ id: message.guild.id });
    let command;
    let pos = 0;

    client.commands.forEach(cmd => {
        if (cmd.category === 'moderation') commands.push(cmd);
    });

    function update() {
        let embed = new MessageEmbed().setColor(client.colors.invisible);
        let txt = '';

        for (let i in commands) {
            i = parseInt(i);
            if (i === pos) txt += `__**${commands[i].name}**__\n`
            else txt += `\`${commands[i].name}\`\n`
        }

        embed.setDescription(txt);

        menu.edit(embed);
    }

    update();

    let emojis = ['⏫', '⏬', '✅'];

    for (let emoji of emojis) {
        menu.react(emoji);
    }

    const filter = (reaction, user) => {
        return user.id === message.author.id && emojis.includes(reaction.emoji.name)
    }

    const collector = await menu.createReactionCollector(filter, { timeout: 120000 });

    collector.on('collect', (reaction, user) => {
        reaction.users.remove(user);
        switch (reaction.emoji.name) {
            case '⏫':
                pos--
                if (pos < 0) pos = commands.length-1;
                update()
                break;

            case '⏬':
                pos++
                if (pos >= commands.length) pos = 0
                update()
                break;

            case '✅':
                menu.reactions.removeAll();
                command = commands[pos];

                let txt = text;

                menu.edit(
                    new MessageEmbed()
                        .setColor(client.colors.invisible)
                        .setDescription(txt)
                );
                break;
        }
    })
}