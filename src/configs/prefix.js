const {MessageEmbed} = require('discord.js');

module.exports = async (client, message, text, menu) => {
    let embed = new MessageEmbed().setColor(client.colors.invisible).setDescription(text.prefix.newPrefix.replace('{emoji}', client.signs.loading));

    menu.edit(embed);

    const filter = (msg) => {
        return msg.author.id === message.author.id;
    }

    const collector = message.channel.createMessageCollector(filter, { timeout: 30000 });

    collector.once('collect', async (message) => {
        message.delete();

        let newPref = message.content;

        if (newPref.length > 5) return message.channel.send(
            new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(text.prefix.tooLong.replace('{emoji}', client.signs.warning))
        );

        menu.edit(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(text.prefix.confirm.replace('{emoji}', client.signs.warning).replace('{newPref}', newPref))
        );

        let emojis = ['✅', '❌'];

        for (let i of emojis) {
            menu.react(i)
        }

        const filter = (reaction, user) => {
            return user.id === message.author.id && emojis.includes(reaction.emoji.name);
        }

        const confirm = menu.createReactionCollector(filter, { timeout: 30000 });

        confirm.once('collect', async (reaction, user) => {
            menu.reactions.removeAll();
            switch (reaction.emoji.name) {
                case '❌':
                    menu.edit(
                        new MessageEmbed()
                            .setColor(client.colors.invisible)
                            .setDescription(text.cancelled.replace('{emoji}', client.signs.no))
                    );
                    break;

                case '✅':
                    let config = await client.models.guild.findOne({ id: message.guild.id });
                    config.prefix = newPref
                    console.log(config);
                    await config.updateOne(config);
            }
        })
    })
}