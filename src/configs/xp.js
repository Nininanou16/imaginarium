const {MessageEmbed} = require('discord.js');

module.exports = async (client, message, text, menu) => {
    let guildConfig = await client.models.guild.findOne({ id: message.guild.id });

    let Switch;
    switch (guildConfig.xp.enabled) {
        case true:
            Switch = client.signs.off
            break;

        default:
            Switch = client.signs.on
    }

    let emojis = [Switch, client.signs.plus, client.signs.minus];

    async function update() {
        guildConfig = await client.models.guild.findOne({ id: message.guild.id });

        if (!guildConfig.xp.roles) guildConfig.xp.roles = {}
        let length = Object.keys(guildConfig.xp.roles).length

        switch (guildConfig.xp.enabled) {
            case true:
                Switch = client.signs.off
                break;

            default:
                Switch = client.signs.on
        }

        emojis[0] = Switch;

        menu.edit(
            new MessageEmbed()
                .setColor(client.colors.invisible)
                .setDescription(`Système d'xp : ${guildConfig.xp.enabled ? client.signs.on : client.signs.off}
            Roles récompense : ${length}
            
            ${guildConfig.xp.enabled ? `${client.signs.off} Désactiver` : `${client.signs.on} Activer`} le système d'XP
            ${client.signs.plus} Ajouter un role de récompense
            ${client.signs.minus} Enlever un role de récompense`)
        );

        menu.reactions.removeAll()

        for (let emoji of emojis) {
            menu.react(emoji);
        }
    }

    function findRole(text) {
        let guild = message.guild;

        let role = guild.roles.cache.get(text);
        if (!role) role = guild.roles.cache.find(r => r.name.startsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name.toLowerCase().startsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name.endsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name.toLowerCase().endsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name === text);

        return role;
    }

    async function getRoles() {
        let roles = await client.models.guild.findOne({ id: message.guild.id });

        roles = roles.xp.roles;
        if (!roles) roles = {}

        return roles;
    }

    update();

    const filter = (reaction, user) => {
        return (emojis.includes(reaction.emoji) || emojis.includes(reaction.emoji.name)) && user.id === message.author.id
    }

    const collector = menu.createReactionCollector(filter, { timeout: 30000 });

    let role;

    collector.on('collect', async (reaction, user) => {
        await reaction.users.remove(user);

        let guildRoles = await getRoles()

        let userFilter = (msg) => {
            return msg.author.id === message.author.id;
        }

        let step = 0;

        switch (reaction.emoji.name) {
            case 'switch_off':
            case 'switch_on':
                switch (guildConfig.xp.enabled) {
                    case true:
                        guildConfig.xp.enabled = false;
                        await guildConfig.updateOne(guildConfig);
                        update();
                        break;

                    default:
                        guildConfig.xp.enabled = true;
                        await guildConfig.updateOne(guildConfig);
                        update();
                        break;
                }
                break;

            case 'plus':
                const add = message.channel.createMessageCollector(userFilter, { timeout: 30000 });

                menu.edit(new MessageEmbed().setDescription('Quel est le nom du role'));

                add.on('collect', async (msg) => {
                    msg.delete();
                    switch (step) {
                        case 0:
                            role = findRole(msg.content);

                            if (!role) return menu.edit(new MessageEmbed().setDescription('Aucun role avec ce nom n\'a été trouvé'));
                            step++
                            menu.edit(new MessageEmbed().setDescription('Quel est le niveau requis pour obtenir ce role ?'));
                            break;

                        case 1:
                            let required = parseInt(msg.content);

                            console.log(required);
                            console.log(required.toString())

                            if (!required) return menu.edit(new MessageEmbed().setDescription('Nombre invalide !'));

                            guildRoles[required.toString()] = role;

                            guildConfig.xp.roles = guildRoles;

                            await guildConfig.updateOne(guildConfig);

                            console.log(guildConfig.xp.roles)

                            add.stop();

                            menu.edit(new MessageEmbed().setDescription(`Role ajouté :\n\nNiveau requis : ${required}\nRole : ${role}`));
                            setTimeout(() => {
                                update()
                            }, 3000)
                    }
                })
                break;

            case 'minus':
                menu.edit(new MessageEmbed().setDescription('Quel est le niveau requis pour l\'obtention du role que vous souhaitez supprimer ?'));

                const remove = message.channel.createMessageCollector(userFilter, { timeout: 30000 });

                remove.once('collect', async (msg) => {
                    let lvl = parseInt(msg);

                    let roleID = guildConfig.xp.roles[lvl.toString()];
                    let role = message.guild.roles.cache.get(roleID);

                    if (!role) {
                        menu.edit(new MessageEmbed().setDescription('Aucun role n\'est attribué à ce niveau, ou le role n\'existe plus'))
                        setTimeout(() => {
                            update()
                        }, 3000)
                    }

                    delete guildConfig.xp.roles[lvl.toString()];
                    await guildConfig.updateOne(guildConfig);

                    menu.edit(new MessageEmbed().setDescription(`Le role ${role} ne sera plus donné au niveau ${lvl}`));

                    setTimeout(() => {
                        update()
                    }, 3000)
                })
                break;
        }
    })
}