const {MessageEmbed} = require('discord.js');

module.exports = async (client, message, text, menu) => {
    let guildConfig = await client.models.guild.findOne({ id: message.guild.id });
    if (!guildConfig.invites) guildConfig.invites = {};

    let reactions =  [client.signs.plus, client.signs.minus];

    function update() {
        menu.reactions.removeAll()

        let roles = guildConfig.invites;
        if (!roles) roles = {};

        for (let i in roles) {
            let role = message.guild.roles.cache.get(roles[i]);

            if (!role) return;
            roles[i] = role
        }

        let txt = '';

        for (let i in roles) {
            txt += `\n${i} Invitation.s : ${roles[i]}`
        }

        if (!txt) txt = 'Aucun role de récompense'

        txt += `\n\n${client.signs.plus} Ajouter un role de récompense\n${client.signs.minus} Enlever un role de récompense`

        menu.edit(new MessageEmbed().setColor(client.colors.invisible).setDescription(txt));
        for (let i of reactions) {
            menu.react(i)
        }
    }

    update();

    const filter = (reaction, user) => {
        return user.id === message.author.id && reactions.includes(reaction.emoji)
    }

    const messageFilter = (msg) => {
        return msg.author.id === message.author.id
    }

    function findRole(text) {
        let guild = message.guild;

        let role = guild.roles.cache.get(text);
        if (!role) role = guild.roles.cache.find(r => r.name.startsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name.toLowerCase().startsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name.endsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name.toLowerCase().endsWith(text));
        if (!role) role = guild.roles.cache.find(r => r.name === text);

        return role;
    }

    let roleUtil;

    let step = 0;

    const collector = menu.createReactionCollector(filter, { timeout: 30000 });

    collector.on('collect', async (reaction, user) => {
        await menu.reactions.removeAll();

        console.log(reaction.emoji.name)

        switch (reaction.emoji.name) {
            case 'plus':
                const add = message.channel.createMessageCollector(messageFilter, { timeout: 30000 });
                menu.edit(new MessageEmbed().setColor(client.colors.invisible).setDescription('Quel est le nom du role a ajouter ?'))

                add.on('collect', async (msg) => {
                    msg.delete();
                    switch (step) {
                        case 0:
                            let role = findRole(msg.content);
                            if (!role) return menu.edit(
                                new MessageEmbed().setColor(client.colors.invisible).setDescription('Aucun role n\'a été trouvé, veuillez réessayer')
                            );

                            roleUtil = role;

                            step++
                            menu.edit(new MessageEmbed().setDescription('Quel est le nombre d\'invitations requises pour obtenir ce role ?').setColor(client.colors.invisible))
                            break;

                        case 1:
                            let invites = parseInt(msg.content);
                            if (!invites) return menu.edit(
                                new MessageEmbed().setColor(client.colors.invisible).setDescription(`\`${msg.content}\` n'est pas un nombre d'invitation valide !`)
                            );

                            guildConfig.invites[invites.toString()] = roleUtil.id;

                            await guildConfig.updateOne(guildConfig);
                            add.stop();

                            menu.edit(
                                new MessageEmbed().setColor(client.colors.invisible).setDescription(`Nouveau role de récompense :\n\nRole : ${roleUtil}\nInvitations requises : ${invites}`)
                            );

                            setTimeout(() => {
                                update()

                                step = 0
                            }, 3000)
                    }
                })
                break;

            case 'minus':
                const remove = message.channel.createMessageCollector(messageFilter, { timeout: 30000 });
                menu.edit(
                    new MessageEmbed().setColor(client.colors.invisible).setDescription('Quel est le nombre d\'invitation.s requise.s pour obtenir le role a supprimer ?')
                );

                remove.on('collect', async (msg) => {
                    msg.delete();
                    let invite = parseInt(msg.content);
                    if (!invite) return menu.edit(
                        new MessageEmbed().setColor(client.colors.invisible).setDescription('Cet nombre d\'invitations est invalide, veuillez réesayer !')
                    );

                    let toRemove = guildConfig.invites[invite.toString()];
                    if (!toRemove) {
                        remove.stop();
                        menu.edit(
                            new MessageEmbed().setColor(client.colors.invisible).setDescription(`Il n'y a aucun role de récompense pour \`${invite}\` invitations !`)
                        );

                        setTimeout(() => {
                            update()
                        }, 3000)
                    }

                    console.log(guildConfig.invites)
                    console.log(invite.toString())
                    console.log(guildConfig.invites[invite.toString()])

                    let role = message.guild.roles.cache.get(guildConfig.invites[invite.toString()]);
                    console.log(role)
                    if (!role) {
                        remove.stop();
                        menu.edit(
                            new MessageEmbed().setColor(client.colors.invisible).setDescription('Le role correspondant n\'est plus valide !')
                        );

                        setTimeout(() => {
                            update()
                        }, 3000)
                    }

                    delete guildConfig.invites[invite.toString()];
                    await guildConfig.updateOne(guildConfig);

                    menu.edit(
                        new MessageEmbed().setColor(client.colors.invisible).setDescription(`Le role ${role} ne sera plus donné lors du passage des \`${invite}\` invitations !`)
                    );
                    remove.stop();

                    setTimeout(() => {
                        update()
                    }, 3000)
                })
                break;
        }
    })
}