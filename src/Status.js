const express = require('express')

module.exports = () => {
    let app = express();

    app.listen(5000);

    app.get('*', (req, res) => {
        res.json({ status: 'Ok' })
    })
}