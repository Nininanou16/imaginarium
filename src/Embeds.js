const {MessageEmbed} = require('discord.js');

module.exports = {
    error: (client, code, lang) => {
        return new MessageEmbed()
            .setColor(client.colors.red)
            .setDescription(client.langs.get(lang || 'fr').embeds.error.replace('{code}', code));
    },
    logsNotFound: (client, lang) => {
        return new MessageEmbed()
            .setColor(client.colors.orange)
            .setDescription(client.langs.get(lang || 'fr').embeds.logsNotFound)
    },
    missingPerm: {
        client: (client, permission, lang) => {
            return new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(client.langs.get(lang || 'fr').embeds.missingPerm.client.replace('{perm}', permission.toLocaleLowerCase()).replace('{emoji}', client.signs.warning))
        },
        userPerm: (client, permission, lang) => {
            return new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(client.langs.get(lang || 'fr').embeds.missingPerm.user.replace('{perm}', permission).replace('{emoji}', client.signs.warning))
        },
        userRole: (client, role, lang) => {
            return new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(client.langs.get(lang || 'fr').embeds.missingPerm.role.replace('{role}', role.name).replace('{emoji}', client.signs.warning))
        },
        userIsHigher: (client, user, lang) => {
            return new MessageEmbed()
                .setColor(client.colors.orange)
                .setDescription(client.langs.get(lang || 'fr').embeds.missingPerm.higherUser.replace('{user}', user.user.tag).replace('{emoji}', client.signs.warning))
        }
    },
    notAllowed: (client, command, lang) => {
        return new MessageEmbed()
            .setColor(client.colors.red)
            .setDescription(client.langs.get(lang || 'fr').embeds.notAllowed.replace('{cmd}', command).replace('{emoji}', client.signs.no))
    },
    moderation: {
        kick: (client, member, lang) => {
            return new MessageEmbed()
                .setColor(client.colors.orange)
        }
    },
    logs: {
        antiRaid: {
            antiInvite: (client, message, link, lang) => {
                return new MessageEmbed()
                    .setColor(client.colors.red)
                    .setDescription(client.langs.get(lang || 'fr').embeds.logs.antiRaid.antiInvite.split('{link}').join(link).replace('{channel}', message.channel).replace('{user}', message.author.tag))
            }
        }
    }
}