module.exports = {
    initGuild: async (client, guild, logsID) => {
        let base = require('./models/guildModel.json');

        base.id = guild.id;
        base.logs.voiceChannel = base.logs.memberChannel = base.logs.serverChannel = base.logs.antiRaidChannel = logsID

        let guildDb = new client.models.guild(base);
        client.console.log('New guild :')
        console.log(guildDb)
        await guildDb.save(err => {
            if (!err) return guildDb;
            client.functions.error(client, err);
            return err;
        })
    },
    initUserXP: async (client, guild, user) => {
        let save = new client.models.userXP({
            userID: user.id,
            guildID: guild.id,
            lvl: 0,
            xp: 0
        });

        await save.save(err => {
            if (!err) return save;
            client.functions.error(client, err);
            return err;
        })
    },
    initUserInvites: async (client, guild, user) => {
        let save = new client.models.userInvites({
            userID: user.id,
            guildID: guild.id,
            invites: {
                bonus: 0,
                true: 0,
                fake: 0
            }
        });

        await save.save(err => {
            if (!err) return save;
            client.functions.error(client, err);
            return err;
        })
    },
    findUser: (search, guild) => {
        let user = guild.members.cache.get(search); // search : id;
        if (!user) user = guild.members.cache.find(usr => usr.user.username === search) // search : username
        if (!user) user = guild.members.cache.find(usr => usr.user.username.toLowerCase() === search) // search : username lower case
        if (!user) user = guild.members.cache.find(usr => usr.user.username.startsWith(search)) // search : username start
        if (!user) user = guild.members.cache.find(usr => usr.user.username.toLowerCase().startsWith(search)) // search: username start lower case
        if (!user) user = guild.members.cache.find(usr => usr.user.username.endsWith(search)) // search username end
        if (!user) user = guild.members.cache.find(usr => usr.user.username.toLowerCase().endsWith(search)) // search username end lower case
        if (!user) user = guild.members.cache.find(usr => usr.user.tag === search) //search tag
        if (!user) user = guild.members.cache.find(usr => usr.user.tag.toLowerCase() === search) //search tag lower case
        if (!user) user = guild.members.cache.find(usr => usr.user.discriminator === search) //search #

        return user;
    },
    error: (client, error) => {
        client.console.error(error);
    },
    scanMsg: async (client, message) => {
        let scanInvite = /discord(?:\.com|app\.com|\.gg)[\/invite\/]?(?:[a-zA-Z0-9\-]{2,32})/ig;
        let scanLink = /(?:(?:https?|ftp):\/\/|\b(?:[a-z\d]+\.))(?:(?:[^\s()<>]+|\((?:[^\s()<>]+|(?:\([^\s()<>]+\)))?\))+(?:\((?:[^\s()<>]+|(?:\(?:[^\s()<>]+\)))?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))?/ig;

        let guildConfig = await client.models.guild.findOne({ id: message.guild.id });
        if (!guildConfig) return;

        if (message.content.match(scanInvite)) {
            if ((guildConfig.antiRaid.antiLink && !guildConfig.antiRaid.antiLinkWL.includes(message.channel.id)) || (guildConfig.antiRaid.antiInvite && !guildConfig.antiRaid.antiInviteWL.includes(message.channel.id))) {
                message.delete();

                message.channel.send('Anti Invite').then(msg => msg.delete({ timeout: 5000 }));

                if (guildConfig.logs.antiInvite) {
                    let channel = client.channels.cache.get(guildConfig.logs.antiRaid);
                    if (!channel) return message.guild.owner.user.send(
                        client.embeds.logsNotFound(client, guildConfig.lang)
                    );

                    channel.send('anti invite');
                }
            }
        }

        if (message.content.match(scanLink)) {
            if (guildConfig.antiRaid.antiLink && !guildConfig.antiRaid.antiLinkWL.includes(message.guild.id)) {
                message.delete();

                message.channel.send('Anti link').then(msg => msg.delete({ timeout: 5000 }));

                if (guildConfig.logs.antiRaid.antiLink) {
                    let channel = client.channels.cache.get(guildConfig.logs.antiRaidChannel);
                    if (!channel) return message.guild.owner.user.send(
                        client.embeds.logsNotFound(client, guildConfig.lang)
                    );

                    channel.send('anti link');
                }
            }
        }
    },
    compareUser: (one, two) => {
        return (one.roles.highest.position > two.roles.highest.position);
    }
}