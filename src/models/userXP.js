const {Schema} = require('mongoose');

module.exports = new Schema({
    userID: Number,
    guildID: Number,
    lvl: Number,
    xp: Number,
    rankBg: String,
    rankColor: String,
    settings: {
        bgColor: String,
        bgImage: String,
        mainColor: String
    }
})