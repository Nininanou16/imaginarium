const mongoose = require('mongoose');

let guildModel = new mongoose.Schema({
    id: Number,
    prefix: String,
    lang: {
        type: String,
        default: 'fr'
    },
    logs: {
        voice: {
            join: Boolean,
            leave: Boolean,
            create: Boolean,
            delete: Boolean,
            selfMute: Boolean,
            selfDeafen: Boolean,
            serverMute: Boolean,
            serverDeafen: Boolean,
            move: Boolean,
            afkChannel: Boolean
        },
        voiceChannel: String,
        server: {
            join: Boolean,
            leave: Boolean,
            createChannel: Boolean,
            deleteChannel: Boolean,
            boost: Boolean,
            unboost: Boolean,
            createRole: Boolean,
            deleteRole: Boolean,
            nameEdit: Boolean,
            avatarEdit: Boolean,
            vanityURLEdit: Boolean,
            ownerEdit: Boolean
        },
        serverChannel: String,
        member: {
            roleAdd: Boolean,
            roleRemove: Boolean,
            ban: Boolean,
            kick: Boolean,
            avatarEdit: Boolean,
            usernameEdit: Boolean,
            nicknameEdit: Boolean,
            statusEdit: Boolean
        },
        memberChannel: String,
        antiRaid: {
            antiSpam: Boolean,
            antiSelfBot: Boolean,
            antiToken: Boolean,
            antiWebHook: Boolean,
            antiLink: Boolean,
            antiInvite: Boolean,
            antiChannelDelete: Boolean,
            antiChannelCreate: Boolean,
            antiEveryone: Boolean,
            antiMassBan: Boolean,
            antiMassKick: Boolean,
            captchaPass: Boolean,
            captchaFail: Boolean
        },
        antiRaidChannel: String
    },
    config: {
        moderation: {
            kick: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'KICK_MEMBERS'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            },
            ban: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'BAN_MEMBERS'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            },
            tempban: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'BAN_MEMBERS'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            },
            unban: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'BAN_MEMBERS'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            },
            mute: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'MANAGE_MESSAGES'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            },
            tempMute: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'MANAGE_MESSAGES'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            },
            unmute: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'MANAGE_MESSAGES'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            },
            clear: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'MANAGE_MESSAGES'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            }
        },
        setup: {
            config: {
                requirement: {
                    type: String,
                    default: 'perm'
                },
                permission: {
                    type: String,
                    default: 'MANAGE_GUILD'
                },
                role: Number,
                activated: {
                    type: Boolean,
                    default: true
                }
            }
        }
    },
    antiRaid: {
        antiLink: Boolean,
        antiLinkWL: Array,
        antiInvite: Boolean,
        antiInviteWL: Array
    },
    xp: {
        enabled: Boolean,
        roles: Object
    },
    invites: Object
}, { collection: 'completed'});

module.exports = guildModel;