const {Schema} = require('mongoose');

module.exports = new Schema({
    userID: Number,
    guildID: Number,
    invites: {
        bonus: Number,
        true: Number,
        fake: Number
    }
})