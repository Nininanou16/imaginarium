const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
    id: Number,
    author: Number,
    subject: String
}, { collection: 'completed' })