const {readdir} = require("fs");
const {WebhookClient} = require('discord.js');

module.exports = (client) => {
    client.Token = client.login(client.settings.token);

    readdir('./src/Events/', (err, files) => {
        if (err) client.emit('error', (err));

        files.forEach(file => {
            let name = file.split('.')
            if (name[1] !== 'js') return;
            const event = require(`./Events/${file}`)
            client.on(name[0], event.bind(null, client));
        });
    });

    let frWebhook = new WebhookClient(client.settings.webhookFrID, client.settings.webhookFrToken);
    let enWebhook = new WebhookClient(client.settings.webhookEnID, client.settings.webhookEnToken);

    if (!client.settings.true) return;

    if (frWebhook) {
        frWebhook.send(`:green_circle: Bot en ligne !`)
    }
    if (enWebhook) {
        enWebhook.send(`:green_circle: Bot online!`)
    }
}
