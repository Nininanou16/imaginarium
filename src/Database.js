// const {MongoClient} = require('mongodb');
//
// module.exports = async (client) => {
//     // client.db = require('quick.db')
//     client.db = new MongoClient(`mongodb+srv://${client.settings.db.username}:${client.settings.db.password}@localhost:27017/?poolSize=20&writeConcern=majority&authMechanism=DEFAULT`, {
//         useNewUrlParser: true,
//         useUnifiedTopology: true
//     });
//
//     await client.db.connect();
// }

const mongoose = require('mongoose');

module.exports = (client) => {
    // client.db = require('quick.db');
    client.models = {
        guild: new mongoose.model('Guild', require('./models/guildModel')),
        ticket: new mongoose.model('Ticket', require('./models/ticketModel')),
        tempmute: new mongoose.model('TempMute', require('./models/tempmuteModel')),
        userXP: new mongoose.model('UserXP', require('./models/userXP')),
        userInvites: new mongoose.model('UserInvites', require('./models/userInvites'))
    };

    mongoose.connection.on('connected', () => { client.console.info('Database connected')});

    mongoose.connect(`mongodb://${client.settings.db.username}:${client.settings.db.password}@127.0.0.1:27017`, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
        if (err) {
            client.functions.error(client, err)
        }
    });

    client.db = mongoose;
}