const {Client} = require("discord.js");
let client = new Client()
require('./src/variables')(client);
require('./src/Bot')(client);
require('./src/Status')();
// require('./src/dashboard')(client);
require('./src/Database')(client);
module.exports = client;