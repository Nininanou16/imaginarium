const {ShardingManager} = require('discord.js');
require('./src/Status')();

const manager = new ShardingManager('./index.js', {
    token: require('./settings.json').token,
    totalShards: 2
});

const client = require('./index')

manager.on('sharCreate', (shard) => {
    client.emit('shardCreate', shard);
    console.log(`Created shard : ${shard.id}`)
});

manager.spawn();